import pydicom
import yaml
import os

import numpy as np

from datetime import datetime
from glob import glob
from rt_utils import RTStructBuilder


class GenerateRTStructure():
    """
    this class will auto generate empty structure to a RS file.
    the defalt output destination is inside the source series dicom path.
    """
    def __init__(self, st):
        """
        initialize function with requied elements.

        Args:
            st (Dict): settings from assets.settings
        """
        self.st = st
        self.yaml_dir = "./assets/meta.yaml"
        self.oar_dict = self._load_yaml()
        self.date = datetime.today().strftime('%Y-%m-%d')

    def __call__(self, series_dir):
        """_summary_

        Args:
            series_dir (_type_): _description_
        """
        dcms = glob(os.path.join(series_dir, "*dcm"))
        sample_ds = pydicom.read_file(dcms[0])
        rtstruct = self.generate_rs(series_dir,
                                    len(dcms),
                                    sample_ds.Rows,
                                    sample_ds.Columns)
        self.set_structure_set_label(rtstruct)
        self.set_roi_type(rtstruct)
        self.save_rs(series_dir, rtstruct)

    def _load_yaml(self):
        """
        Returns:
            oar_dict (Dict[str:Tuple[int]]): create a Dict that contain the
                                             target ROInames and the colors.
        """
        with open(self.yaml_dir, "r") as f:
            oar_settings = yaml.safe_load(f)
        oar_dict = {}
        for oar in oar_settings['organs']:
            rgb = self.hex_to_rgb(oar_settings['org_cont_color'][oar])
            oar_dict.update({f"{oar}": rgb})
        return oar_dict

    def hex_to_rgb(self, value):
        """convert hex to rbg

        Args:
            value (str): input hex value.

        Returns:
            Tuple[int]: rgb value.
        """
        value = value.lstrip('#')
        lv = len(value)
        rgb = []
        for i in range(0, lv, lv // 3):
            rgb.append(int(value[i:i + lv // 3], 16))
        return rgb

    def generate_rs(self, series_dir, len_samples, Rows, Columns):
        """generage a rs dataset by RTStructBuilder.create_new

        Args:
            series_dir (str): single series dicom directory.
            len_samples (int): length of series dicom.
            Rows (int): width of image.
            Columns (int): hight of image.

        Returns:
            RTStructBuilder.Dataset: RTStructBuilder format dataset.
        """
        empty_mask = np.zeros((Rows, Columns, len_samples), dtype=bool)
        rtstruct = RTStructBuilder.create_new(dicom_series_path=series_dir)
        for oar, clr in self.oar_dict.items():
            rtstruct.add_roi(
                mask=empty_mask,
                color=list(clr),
                name=oar,
                description=f"{self.date} {self.st.ROIDescription}",
                use_pin_hole=False,
                approximate_contours=True,
                # AUTOMATIC, SEMIAUTOMATIC, MANUAL
                roi_generation_algorithm=self.st.roi_generation_algorithm
            )
        return rtstruct

    def set_structure_set_label(self, rtstruct):
        """set StructureSetLabel, default RS_01

        Args:
            rtstruct (RTStructBuilder.Dataset): RS dataset
        """
        if self.st.StructureSetLabel:
            rtstruct.ds.StructureSetLabel = self.st.StructureSetLabel
        else:
            rtstruct.ds.StructureSetLabel = "RS_01"

    def save_rs(self, series_dir, rtstruct):
        """save DataSet to dst folder.

        Args:
            series_dir (str): if settings.output_dir is none, the default
                              output directory will be the source dir.
            rtstruct (RTStructBuilder.Dataset): rs dataset.
        """
        if self.st.output_dir:
            ref_series_uid = rtstruct.ds[0x30060010][0][0x30060012][0][0x30060014][0][0x0020000e].value
            dst_path = os.path.join(self.st.output_dir, ref_series_uid, "RS")
            os.makedirs(dst_path, exist_ok=True)
            rtstruct.save(f"{dst_path}/RS_{rtstruct.ds.SOPInstanceUID}.dcm")
        else:
            os.makedirs(os.path.join(series_dir, "RS"), exist_ok=True)
            rtstruct.save(
                f"{series_dir}/RS/RS_{rtstruct.ds.SOPInstanceUID}.dcm"
                )

    def set_roi_type(self, rtstruct):
        """set RTROIInterpretedType, the type must be ORGAN.

        Args:
            rtstruct (RTStructBuilder.Dataset): rs dataset.
        """
        for seq in rtstruct.ds[0x30060080]:
            seq.RTROIInterpretedType = "ORGAN"
