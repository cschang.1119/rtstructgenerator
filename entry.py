import pandas as pd

from tqdm import tqdm

from assets.settings import Settings
from core.generator import GenerateRTStructure


if __name__ == "__main__":
    st = Settings()
    rs_generator = GenerateRTStructure(st)
    df = pd.read_csv(st.csv_path)
    for i,row in df.iterrows():
        try:
            rs_generator(row['local_ct'])
        except Exception as err:
            print(err)
