from pydantic import BaseSettings


class Settings(BaseSettings):
    csv_path: str = "directory to csv file"
    ROIDescription: str = "description."
    # AUTOMATIC, SEMIAUTOMATIC, MANUAL
    roi_generation_algorithm: str = "MANUAL"
    StructureSetLabel: str = "RS_01"
    output_dir: str = "output directory."
